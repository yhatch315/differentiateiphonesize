//
//  ViewController.m
//  DifferentiateIphoneSize
//
//  Created by Edward P. Legaspi on 8/1/14.
//  Copyright (c) 2014 Kalidad Biz Solutions. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UILabel *lbl_textOutput;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        if ([[UIScreen mainScreen] scale] == 2.0) {
            if([UIScreen mainScreen].bounds.size.height == 568){
                // iPhone retina-4 inch
                self.lbl_textOutput.text = @"iPhone retina-4 inch";
            } else{
                // iPhone retina-3.5 inch
                self.lbl_textOutput.text = @"iPhone retina-3.5 inch";
            }
        }
        else {
            // not retina display
            self.lbl_textOutput.text = @"iPhone that's not retina display";
        }
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
