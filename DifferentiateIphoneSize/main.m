//
//  main.m
//  DifferentiateIphoneSize
//
//  Created by Edward P. Legaspi on 8/1/14.
//  Copyright (c) 2014 Kalidad Biz Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
